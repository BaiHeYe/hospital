package com.rf.admin.controller;

import com.rf.admin.domain.Register;
import com.rf.admin.service.RegisterService;
import com.rf.common.response.ResponseVo;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/22
 */

@RestController
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @GetMapping("/{orderId}")
    public ResponseVo selectRegister(@PathVariable String orderId) {
        List<Register> registers = registerService.getRegisters(orderId);
        return ResponseVo.SUCCESS(registers);
    }

    @PostMapping("/{userId}")
    public ResponseVo saveRegister(@PathVariable String userId,
                                   @RequestBody Register[] registers ) throws Exception {

        return registerService.saveRegister(userId,registers) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

}
