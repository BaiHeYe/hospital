package com.rf.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 后台用户表
 *
 * @TableName admin_user
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "sys_admin_user")
public class AdminUser {
    /**
     *
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 工号
     */
    private String jobNum;

    /**
     * 姓名
     */
    private String adminName;

    /**
     * 密码
     */
    private String adminPassword;

    /**
     * 盐
     */
    @ApiModelProperty("盐")
    private String salt;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 职位
     */
    private Integer ranks;

    /**
     * 入职日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date entryTime;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 最后修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * token
     */
    private String token;

    /**
     * 是否被删除
     */
    @TableLogic
    private Integer deleted;

    private String photographPath;

}