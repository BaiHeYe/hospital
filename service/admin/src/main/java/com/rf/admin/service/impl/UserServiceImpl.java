package com.rf.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rf.admin.domain.User;
import com.rf.admin.domain.vague.Vague;
import com.rf.admin.mapper.UserMapper;
import com.rf.admin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;


/**
 * @Description
 * @Author rf
 * @Date 2022/4/11
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 条件/分页查询
     */
    @Override
    public IPage<User> selectUser(Integer pageCurrent, Integer pageSize, Vague vague) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                // 模糊查询名字
                .like(StringUtils.isNotBlank(vague.getName()), User::getName, vague.getName())
                // 时间区间
                .ge(vague.getTimeBean() != null, User::getCreateTime, vague.getTimeBean())
                .le(vague.getTimeEnd() != null, User::getCreateTime, vague.getTimeEnd());


        Page<User> userPage = new Page<>(pageCurrent, pageSize);
        return userMapper.selectPage(userPage, queryWrapper);
    }

    /**
     * 添加用户
     */
    @Override
    public boolean addUser(User user) {
        int result = 0;
        if (user != null) {
            result = userMapper.insert(user);
        }
        return result > 0;
    }

    /**
     * 按照ID更新用户
     */
    @Override
    public boolean updateByIdUser(User user) {
        int result = 0;
        if (user != null) {
            result = userMapper.updateById(user);
        }
        return result > 0;

    }

    /**
     * 按照ID删除用户
     */
    @Override
    public boolean deleteByIdUser(String id) {
        int result = 0;
        if (id != null) {
            result = userMapper.deleteById(id);
        }
        return result > 0;
    }

    @Override
    public boolean selectUserPayPassword(String id,Integer password) {
        User user = userMapper.selectById(id);
        return Objects.equals(user.getPayPassword(), password);
    }


}
