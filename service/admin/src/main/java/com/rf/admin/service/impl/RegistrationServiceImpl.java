package com.rf.admin.service.impl;

import com.rf.admin.domain.Registration;
import com.rf.admin.domain.vague.Department;
import com.rf.admin.mapper.RegistrationMapper;
import com.rf.admin.service.DepartService;
import com.rf.admin.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_registration(挂号表)】的数据库操作Service实现
 * @createDate 2022-04-24 15:03:26
 */
@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private RegistrationMapper registrationMapper;

    @Autowired
    private DepartService departService;

    /**
     * 实现每个门诊下的医生
     * 每个门诊下医生挂号价格、挂号数量、医生信息
     */
    @Override
    public List<Department> selectRegistration() {
        List<Department> listDepart = departService.getAllDept();
        for (Department department : listDepart) {
            List<Department> deptRegistration = getDeptRegistration(department.getId());
            department.setChildren(deptRegistration);
        }
        return listDepart;
        // 所有部门
//        List<Depart> allDept = departService.getAllDept();
//        List<Object> list = new ArrayList<>();
//        if (allDept.size() > 0){
//            for (Depart depart : allDept) {
//                list.add(depart.getDepartName());
//                list.add(getDeptRegistration(depart.getId()));
//            }
//        }
//        return list;

//        List<Department> departmentList = new ArrayList<>();
//        if (allDept.size() > 0 ){
//            Department depart = new Department();
//            for (int i = 0; i < allDept.size(); i++) {
//                departmentList.get(i).setName(allDept.get(i).getDepartName());
//                List<Registration> deptRegistration = getDeptRegistration(allDept.get(i).getId());
//                departmentList.get(i).setList(deptRegistration);
//            }
//        }
//        return departmentList;
    }

    /**
     *  按照部门ID查询用户
     */
    public List<Department> getDeptRegistration(String deptId){
        return registrationMapper.selectRegistrationUser(deptId);
    }

    /**
     * 按照id查询门诊挂号信息医生信息
     */
    public Registration getDeptRegistrationUser(String id){
        return registrationMapper.selectDeptRegistrationUser(id);
    }

    @Override
    public boolean delRegistration(String id) {
        return registrationMapper.deleteById(id) > 0;
    }

    @Override
    public boolean installRegistration(Registration registration) {
        return registrationMapper.insert(registration) > 0;
    }

    @Override
    public boolean updateRegistration(Registration registration) {
        return registrationMapper.updateById(registration) > 0;
    }

    @Override
    public Registration getAllRegistration(String id) {
        return registrationMapper.selectById(id);
    }


}
