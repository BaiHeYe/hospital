package com.rf.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rf.admin.domain.User;
import com.rf.admin.domain.vague.IdAndName;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/11
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {



}
