package com.rf.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 订单表
 *
 * @TableName sys_order
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    /**
     *
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 用户id
     */
    private String userId;

    /**
     * 产品id
     */
    private String orderId;

    /**
     * 订单总金额
     */
    private Integer orderAmount;

    /**
     * 支付状态，默认0未支付
     */
    private Integer payStatus;

    /**
     * 订单是否出库
     */
    private Integer orderStatus;

    /**
     * 订单信息
     */
    private String orderInfo;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 删逻辑除
     */
    @TableLogic
    private Integer deleted;


    private Integer version;

    private User user;

    private AdminUser admin;

    public Order(String userId, Integer orderAmount, String orderInfo) {
        this.userId = userId;
        this.orderAmount = orderAmount;
        this.orderInfo = orderInfo;
    }
}