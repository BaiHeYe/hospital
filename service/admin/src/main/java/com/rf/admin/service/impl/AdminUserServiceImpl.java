package com.rf.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rf.admin.domain.AdminUser;
import com.rf.admin.domain.vague.IdAndName;
import com.rf.admin.domain.vague.Vague;
import com.rf.admin.mapper.AdminUserMapper;
import com.rf.admin.service.AdminUserService;
import com.rf.common.utils.Md5Utils;
import com.rf.common.vo.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/14
 */
@Service
public class AdminUserServiceImpl implements AdminUserService {

    @Autowired
    private AdminUserMapper adminUserMapper;

    /**
     * 条件分页查询
     */
    @Override
    public Page<AdminUser> pageAdminUser(Integer pageCurrent, Integer pageSize, Vague vague) {
        LambdaQueryWrapper<AdminUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper// 模糊查询名字
                .like(StringUtils.isNotBlank(vague.getName()), AdminUser::getAdminName, vague.getName())
                .like(vague.getJobNum() != null, AdminUser::getJobNum, vague.getJobNum())
                // 时间区间
                .ge(vague.getTimeBean() != null, AdminUser::getCreateTime, vague.getTimeBean())
                .le(vague.getTimeEnd() != null, AdminUser::getCreateTime, vague.getTimeEnd())
                .orderByDesc(AdminUser::getCreateTime);
        Page<AdminUser> pageAdmin = new Page<>(pageCurrent, pageSize);
        return adminUserMapper.selectPage(pageAdmin, queryWrapper);
    }

    @Override
    public boolean insertAdminUser(AdminUser adminUser) {
        int result = 0;
        if (adminUser != null) {
            if (StringUtils.isBlank(adminUser.getPhotographPath())) {
                adminUser.setPhotographPath("http://r9nllsa01.hb-bkt.clouddn.com/17b4cfdf-6658-4836-9128-51070644d7fb-%E6%B8%B8%E6%88%8F%E6%9C%BA%E7%94%B7%E5%AD%A9.png");
            }
            String salt = Md5Utils.getSalt(8);
            String password = Md5Utils.getMd5("123456", salt);
            adminUser.setAdminPassword(password);
            adminUser.setSalt(salt);
            result = adminUserMapper.insert(adminUser);
        }
        return result > 0;
    }

    @Override
    public boolean delByIdAdminUser(String id) {
        int result = 0;
        if (id != null) {
            result = adminUserMapper.deleteById(id);
        }
        return result > 0;
    }

    @Override
    public boolean updateByIdAdminUser(AdminUser adminUser) {
        int result = 0;
        if (adminUser != null) {
            result = adminUserMapper.updateById(adminUser);
        }
        return result > 0;
    }

    @Override
    public AdminUser findByAdminName(String username) {
        if (username != null) {
            QueryWrapper<AdminUser> qw = new QueryWrapper<>();
            qw.eq("admin_name", username);
            return adminUserMapper.selectOne(qw);
        }
        return null;
    }

    @Override
    public AdminUser findById(String id) {
        if (id != null) {
            QueryWrapper<AdminUser> qw = new QueryWrapper<>();
            qw.eq("id", id);
            return adminUserMapper.selectOne(qw);
        }
        return null;
    }

    @Override
    public List<IdAndName> getIdAndName() {
        return adminUserMapper.IdAndName();
    }

    @Override
    public boolean updatePassword(String id, Email email) {
        String salt = Md5Utils.getSalt(8);
        String password = Md5Utils.getMd5(email.getPassword(), salt);
        AdminUser adminUser = new AdminUser();
        adminUser.setId(id);
        adminUser.setSalt(salt);
        adminUser.setAdminPassword(password);
        return adminUserMapper.updateById(adminUser) >= 1;
    }

}
