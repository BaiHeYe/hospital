package com.rf.admin.mapper;
import java.util.List;

import com.rf.admin.domain.vague.Department;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rf.admin.domain.Depart;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Mr.R
 * @description 针对表【sys_depart(部门字典表)】的数据库操作Mapper
 * @createDate 2022-04-24 15:25:57
 * @Entity com.rf.admin.domain.SysDepart
 */
@Mapper
public interface DepartMapper extends BaseMapper<Depart> {

    List<Department> getAll();

}
