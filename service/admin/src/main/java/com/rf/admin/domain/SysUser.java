package com.rf.admin.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 患者用户表
 * @TableName sys_user
 */
@TableName(value ="sys_user")
@Data
public class SysUser implements Serializable {
    /**
     * 患者ID,后台生成uuid
     */
    @TableId
    private String id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 盐
     */
    private String salt;

    /**
     * 年龄
     */
    private String age;

    /**
     * 性别
     */
    private String gender;

    /**
     * 余额
     */
    private Double balance;

    /**
     * 身份证
     */
    private Long identity;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后修改时间
     */
    private Date updateTime;

    /**
     * 支付密码，后端判断只能六位
     */
    private Integer payPassword;

    /**
     * 已认证 0未认证，1已认证
     */
    private Integer certified;

    /**
     * 是否被删除 0未删除 1已删除
     */
    private Integer deleted;

    /**
     * 头像地址，有默认以后再加
     */
    private String photographPath;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}