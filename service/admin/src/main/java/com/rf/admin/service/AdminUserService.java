package com.rf.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rf.admin.domain.AdminUser;
import com.rf.admin.domain.vague.IdAndName;
import com.rf.admin.domain.vague.Vague;
import com.rf.common.vo.Email;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【admin_user(后台用户表)】的数据库操作Service
 * @createDate 2022-04-14 09:16:39
 */
public interface AdminUserService {

    Page<AdminUser> pageAdminUser(Integer pageCurrent, Integer pageSize, Vague vague);

    boolean insertAdminUser(AdminUser adminUser);

    boolean delByIdAdminUser(String id);

    boolean updateByIdAdminUser(AdminUser adminUser);

    AdminUser findByAdminName(String username);

    AdminUser findById(String id);

    List<IdAndName> getIdAndName();

    boolean updatePassword(String id, Email email);

}
