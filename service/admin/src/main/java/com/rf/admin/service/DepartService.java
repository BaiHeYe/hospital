package com.rf.admin.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rf.admin.domain.Depart;
import com.rf.admin.domain.vague.Department;
import com.rf.admin.domain.vague.Vague;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_depart(部门字典表)】的数据库操作Service
 * @createDate 2022-04-24 15:25:57
 */
public interface DepartService {

    Page<Depart> getPageDept(Integer pageCurrent, Integer pageSize, Vague vague);

    List<Department> getAllDept();

    boolean delDept(String id);

    boolean updateDept(Depart depart);

    boolean insertDept(Depart depart);

}
