package com.rf.admin.domain.vague;

import com.rf.admin.domain.Registration;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


/**
 * @Description
 * @Author rf
 * @Date 2022/4/25
 */
@Data
public class Department {

    private String id;

    private String RegistrationId;

    private String name;

    // 门诊下的医生
    private List<Department> children = new ArrayList<>();

}
