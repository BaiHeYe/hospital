package com.rf.admin.controller;

import com.rf.admin.domain.UserAddress;
import com.rf.admin.service.UserAddressService;
import com.rf.common.response.ResponseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/11
 */
@RestController
@RequestMapping("/address")
@Api(tags = "地址控制类")
public class UserAddressController {

    @Autowired
    private UserAddressService userAddressService;

    @ApiOperation("分页地址信息")
    @GetMapping("/{id}")
    public ResponseVo selectPageAddress(@PathVariable String id) {
        List<UserAddress> userAddressIPage = userAddressService.selectAddress(id);
        return ResponseVo.SUCCESS(userAddressIPage);
    }

    @ApiOperation("按照ID删除地址")
    @DeleteMapping("/{id}")
    public ResponseVo delAddress(@ApiParam("地址ID") @PathVariable String id) {
        return userAddressService.delByIdAddress(id) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @ApiOperation("修改地址")
    @PutMapping
    public ResponseVo updateAddress(@ApiParam("地址信息") @RequestBody UserAddress userAddress) {
        System.out.println(userAddress);
        return userAddressService.updateByIdAddress(userAddress) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @PostMapping("/save/{id}")
    public ResponseVo saveAddress(@PathVariable String id, @RequestBody UserAddress address) {
        address.setUserId(id);
        return userAddressService.insertAddress(address) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }
}
