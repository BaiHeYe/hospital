package com.rf.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户实体类")
@TableName(value = "sys_user")
public class User {
    /**
     * 用户ID
     */
    @ApiModelProperty("主键ID")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 姓名
     */
    @ApiModelProperty("用户名称")
    private String name;

    /**
     * 邮箱
     */
    @ApiModelProperty("用户邮箱")
    private String email;

    /**
     * 手机号
     */
    @ApiModelProperty("用户手机号")
    private String phone;

    /**
     * 登录密码
     */
    @ApiModelProperty("登录密码")
    private String password;

    /**
     * 盐
     */
    @ApiModelProperty("盐")
    private String salt;
    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private String age;

    /**
     * 性别
     */
    @ApiModelProperty("性别")
    private String gender;

    /**
     * 余额
     */
    @ApiModelProperty("余额")
    private Double balance;

    /**
     * 身份证
     */
    @ApiModelProperty("身份证")
    private Long identity;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 最后修改时间
     */
    @ApiModelProperty("最后修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 支付密码，后端判断只能六位
     */
    @ApiModelProperty("支付密码")
    private Integer payPassword;

    /**
     * 已认证 0未认证，1已认证
     */
    @ApiModelProperty("是否认证")
    private Integer certified;

    /**
     * 是否被删除 0未删除 1已删除
     */
    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Integer deleted;

    /**
     * 头像地址，有默认以后再加
     */
    @ApiModelProperty("头像路径")
    @TableField(fill = FieldFill.INSERT)
    private String photographPath;

}
