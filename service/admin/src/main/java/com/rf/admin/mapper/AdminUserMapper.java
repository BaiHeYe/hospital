package com.rf.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rf.admin.domain.AdminUser;
import com.rf.admin.domain.vague.IdAndName;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/14
 */
@Mapper
public interface AdminUserMapper extends BaseMapper<AdminUser> {

    List<IdAndName> IdAndName();
}
