package com.rf.admin.service;


import com.rf.admin.domain.Depart;
import com.rf.admin.domain.Registration;
import com.rf.admin.domain.vague.Department;

import java.util.List;
import java.util.Map;

/**
 * @author Mr.R
 * @description 针对表【sys_registration(挂号表)】的数据库操作Service
 * @createDate 2022-04-24 15:03:26
 */
public interface RegistrationService {

    List<Department> selectRegistration();

    List<Department> getDeptRegistration(String deptId);

    Registration getDeptRegistrationUser(String id);

    boolean delRegistration(String id);

    boolean installRegistration(Registration registration);

    boolean updateRegistration(Registration registration);

    Registration getAllRegistration(String id);
}
