package com.rf.admin.domain.vague;

import lombok.Data;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/27
 */
@Data
public class IdAndName {

    private String id;

    private String name;

}
