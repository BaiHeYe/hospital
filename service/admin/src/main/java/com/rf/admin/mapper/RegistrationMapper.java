package com.rf.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rf.admin.domain.AdminUser;
import com.rf.admin.domain.Registration;
import com.rf.admin.domain.vague.Department;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_registration(挂号表)】的数据库操作Mapper
 * @createDate 2022-04-24 15:03:26
 * @Entity com.rf.admin.domain.SysRegistration
 */
@Mapper
public interface RegistrationMapper extends BaseMapper<Registration> {

    Registration selectDeptRegistrationUser(String id);

    List<Department> selectRegistrationUser(String deptId);

}
