package com.rf.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rf.admin.domain.Depart;
import com.rf.admin.domain.vague.Vague;
import com.rf.admin.service.DepartService;
import com.rf.common.response.ResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description 部门的陈增删改查
 * @Author rf
 * @Date 2022/4/24
 */
@RestController
@RequestMapping("/dept")
public class DepartController {


    @Autowired
    private DepartService departService;

    @PostMapping("/{pageCurrent}/{pageSize}")
    public ResponseVo listDepart(@PathVariable Integer pageCurrent,
                                 @PathVariable Integer pageSize,
                                 Vague vague) {
        Page<Depart> allDept = departService.getPageDept(pageCurrent, pageSize, vague);
        return ResponseVo.SUCCESS(allDept);
    }

    @DeleteMapping("/{id}")
    public ResponseVo delDepart(@PathVariable String id) {
        return departService.delDept(id) ? ResponseVo.SUCCESS : ResponseVo.FAIL;

    }

    @PostMapping
    public ResponseVo saveDepart(@RequestBody Depart depart) {
        return departService.insertDept(depart) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @PutMapping
    public ResponseVo updateDepart(@RequestBody Depart depart) {
        return departService.updateDept(depart) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

}
