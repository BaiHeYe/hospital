package com.rf.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rf.admin.domain.Register;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_register(用户挂号表)】的数据库操作Mapper
 * @createDate 2022-04-21 20:28:39
 * @Entity com.rf.admin.domain.Register
 */
@Mapper
public interface RegisterMapper extends BaseMapper<Register> {

    List<Register> getRegisters(String orderId);

}
