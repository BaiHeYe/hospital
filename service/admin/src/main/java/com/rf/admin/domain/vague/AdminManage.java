package com.rf.admin.domain.vague;

import lombok.Data;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/24
 */
@Data
public class AdminManage {

    // 系统管理员
    private Integer ADMIN = 0;

    // 医生
    private Integer DOCTOR = 1;

    // 收银员
    private Integer CASHIER = 2;

    // 药剂师
    private Integer PHARMACIST = 3;
}
