package com.rf.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 用户地址
 *
 * @TableName user_address
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("用户地址")
public class UserAddress {

    /**
     * 地址ID
     */
    @ApiModelProperty("地址ID")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 用户ID
     */
    @ApiModelProperty("用户ID")
    private String userId;

    /**
     * 收货人全名
     */
    @ApiModelProperty("收货人全名")
    private String aName;

    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String aPhone;

    /**
     * 省份
     */
    @ApiModelProperty("省份")
    private String aState;

    /**
     * 城市
     */
    @ApiModelProperty("城市")
    private String aCity;

    /**
     * 区/县
     */
    @ApiModelProperty("区/县")
    private String aDistrict;

    /**
     * 详细，如：xx路xx号
     */
    @ApiModelProperty("详细，如：xx路xx号")
    private String aAddress;

    /**
     * 邮政编码,如：110000
     */
    @ApiModelProperty("邮政编码,如：110000")
    private String aZip;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 最后修改时间
     */
    @ApiModelProperty("最后修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 逻辑删除
     */
    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Integer deleted;

    /**
     * 一对多处理
     */
    @ApiModelProperty("一对多处理")
    private User user;
}