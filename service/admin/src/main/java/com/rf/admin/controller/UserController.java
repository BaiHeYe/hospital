package com.rf.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rf.admin.domain.User;
import com.rf.admin.domain.vague.Vague;
import com.rf.admin.service.UserService;
import com.rf.common.response.ResponseVo;
import com.rf.common.utils.QiNiuYunUpFileUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description 用户管理模块
 * @Author rf
 * @Date 2022/4/11
 */
@RestController
@RequestMapping("/user")
@Api(tags = "用户控制类", description = "完成用户的增删改查和头像上传")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "分页用户信息")
    @PostMapping("/{pageCurrent}/{pageSize}")
    public ResponseVo AllUser(@ApiParam("起始页") @PathVariable Integer pageCurrent,
                              @ApiParam("当前页数量") @PathVariable Integer pageSize,
                              Vague vague) {
        IPage<User> userIPage = userService.selectUser(pageCurrent, pageSize, vague);
        if (userIPage != null) {
            return ResponseVo.SUCCESS(userIPage);
        }
        return ResponseVo.FAIL;
    }

    @ApiOperation("按照ID删除用户")
    @DeleteMapping("/{id}")
    public ResponseVo delUser(@ApiParam("用户ID") @PathVariable String id) {
        return userService.deleteByIdUser(id) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @ApiOperation("新增用户")
    @PostMapping
    public ResponseVo addUser(@ApiParam("新增用户信息") User user) {
        return userService.addUser(user) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @ApiOperation("按照ID修改用户")
    @PutMapping
    public ResponseVo updateUser(@ApiParam("修改用户信息") @RequestBody User user) {
        return userService.updateByIdUser(user) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @ApiOperation("上传头像")
    @PostMapping("/photographPath")
    public ResponseVo upload(@ApiParam("用户路径") MultipartFile uploadFile) {
        if (uploadFile != null) {
            return QiNiuYunUpFileUtils.getUpLoad(uploadFile);
        }
        return ResponseVo.FAIL;
    }
}
