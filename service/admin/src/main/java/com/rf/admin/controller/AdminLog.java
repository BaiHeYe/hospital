package com.rf.admin.controller;

import com.rf.admin.domain.AdminUser;
import com.rf.admin.realm.CustomerRealm;
import com.rf.admin.service.AdminUserService;
import com.rf.common.response.ResponseVo;
import com.rf.common.utils.JwtUtils;
import com.rf.common.utils.MailUtils;
import com.rf.common.vo.Email;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/17
 */
@RestController
@RequestMapping("/adminlog")
@Slf4j
public class AdminLog {

    @Autowired
    private AdminUserService adminUserService;

    @Autowired
    private MailUtils mailUtils;

    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 身份认证
     */
    @PostMapping
    public ResponseVo logIn(@RequestBody AdminUser user) {
        Subject subject = SecurityUtils.getSubject();
        // 封装成Token
        try {
            String username = user.getAdminName();
            String password = user.getAdminPassword();
            subject.login(new UsernamePasswordToken(username, password));
            return ResponseVo.SUCCESS(CustomerRealm.getIdAndName());
        } catch (AuthenticationException e) {
            e.printStackTrace();
            log.error("用户名密码错误");
        }
        return ResponseVo.FAIL("认证失败");
    }

    /**
     * 获取登录信息
     */
    @GetMapping("/info")
    public ResponseVo info(HttpServletRequest token) {
        if (JwtUtils.checkToken(token)){
            String AdminId = JwtUtils.getMemberIdByJwtToken(token);
            AdminUser admin = adminUserService.findById(AdminId);
            return ResponseVo.SUCCESS(admin);
        }
        SecurityUtils.getSubject();
        return ResponseVo.FAIL("会话已过期");
    }

    /**
     * 退出登录
     */
    @GetMapping("/logout")
    public ResponseVo logout() {
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.logout();
            return ResponseVo.SUCCESS("退出成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseVo.FAIL("退出失败");
    }

    /**
     * 重置密码
     */
    @PostMapping("/reset")
    public ResponseVo resetPassword(@RequestBody Email email,
                                    HttpServletRequest request) {
        String code = (String) redisTemplate.opsForValue().get(email.getEmail());
        if(Objects.equals(code, email.getEmailCode())){
            String id = JwtUtils.getMemberIdByJwtToken(request);
            return adminUserService.updatePassword(id,email) ? ResponseVo.SUCCESS("更改成功") : ResponseVo.FAIL("更改失败");
        }
        return ResponseVo.FAIL("验证码错误");
    }
    /**
     * 请求发送email
     */
    @PostMapping("/email")
    public ResponseVo resetPassword(@RequestBody Email email) {
        try {
            mailUtils.contextLoads(email.getEmail());
            return ResponseVo.SUCCESS;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
