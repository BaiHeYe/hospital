package com.rf.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rf.admin.domain.UserAddress;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * @Description
 * @Author rf
 * @Date 2022/4/11
 */
@Mapper
public interface UserAddressMapper extends BaseMapper<UserAddress> {

    List<UserAddress> selectAddress(String id);


}
