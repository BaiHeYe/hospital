package com.rf.admin.controller;

import com.rf.admin.domain.Registration;
import com.rf.admin.service.RegistrationService;
import com.rf.common.response.ResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/25
 */
@RestController
@RequestMapping("/registration")
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @GetMapping
    public ResponseVo getDeptRegistration(){
        return ResponseVo.SUCCESS(registrationService.selectRegistration());
    }

    @GetMapping("/deptRegistrationUser/{id}")
    public ResponseVo getDeptRegistrationUser(@PathVariable String id){
        return ResponseVo.SUCCESS(registrationService.getDeptRegistrationUser(id));
    }

    @DeleteMapping("{id}")
    public ResponseVo deleteDeptRegistration(@PathVariable String id){
        return registrationService.delRegistration(id) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @GetMapping("/all/{id}")
    public ResponseVo getAllRegistration(@PathVariable String id){
        return ResponseVo.SUCCESS(registrationService.getAllRegistration(id));
    }

    @PutMapping
    public ResponseVo updateRegistration(@RequestBody Registration registration){
        return registrationService.updateRegistration(registration) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @PostMapping
    public ResponseVo saveRegistration(@RequestBody Registration registration){
        return registrationService.installRegistration(registration) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }
}
