package com.rf.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rf.admin.domain.Drug;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Mr.R
 * @description 针对表【sys_drug(药品表)】的数据库操作Mapper
 * @createDate 2022-04-17 19:30:43
 * @Entity com.rf.admin.domain.Drug
 */
@Mapper
public interface DrugMapper extends BaseMapper<Drug> {


}
