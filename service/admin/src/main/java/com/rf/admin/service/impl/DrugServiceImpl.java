package com.rf.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rf.admin.domain.Drug;
import com.rf.admin.domain.vague.Vague;
import com.rf.admin.mapper.DrugMapper;
import com.rf.admin.service.DrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_drug(药品表)】的数据库操作Service实现
 * @createDate 2022-04-17 19:30:43
 */
@Service
public class DrugServiceImpl implements DrugService {

    @Autowired
    private DrugMapper drugMapper;


    @Override
    public Page<Drug> pageDrug(Integer pageCurrent, Integer pageSize, Vague vague) {
        Page<Drug> drugPage = new Page<>(pageCurrent, pageSize);
        LambdaQueryWrapper<Drug> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper
                // 模糊查询名字
                .like(StringUtils.isNotBlank(vague.getName()), Drug::getDrugName, vague.getName())
                // 模糊查询类型
                .like(StringUtils.isNotBlank(vague.getType()), Drug::getDrugType, vague.getType())
                // 时间区间
                .ge(vague.getTimeBean() != null, Drug::getCreateTime, vague.getTimeBean())
                .le(vague.getTimeEnd() != null, Drug::getCreateTime, vague.getTimeEnd());
        return drugMapper.selectPage(drugPage, queryWrapper);
    }

    @Override
    public boolean delDrug(String id) {
        return drugMapper.deleteById(id) > 0;
    }

    @Override
    public boolean insertDrug(Drug drug) {
        return drugMapper.insert(drug) > 0;
    }

    @Override
    public boolean updateDrug(Drug drug) {
        return drugMapper.updateById(drug) > 0;
    }

    @Override
    public List<Drug> getAllDrug() {
        return drugMapper.selectList(null);
    }

    @Override
    public boolean updateDrugNumber(String id,Integer number) {
        Drug drug = drugMapper.selectById(id);
        Drug drug1 = new Drug();
        drug1.setId(drug.getId());
        drug1.setDrugMoney(drug.getDrugMoney());
        drug1.setDrugNum(drug.getDrugNum() - number);
        return drugMapper.updateById(drug1) > 0;
    }

}
