package com.rf.admin.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rf.admin.domain.User;
import com.rf.admin.domain.vague.Vague;

/**
 * @Description 用户增删改查
 * @Author rf
 * @Date 2022/4/11
 */
public interface UserService {

    /**
     * 分页查询
     */
    IPage<User> selectUser(Integer pageCurrent, Integer pageSize, Vague vague);

    /**
     * 添加用户
     */
    boolean addUser(User user);

    /**
     * 按照ID更新用户
     */
    boolean updateByIdUser(User user);

    /**
     * 按照ID删除用户
     */
    boolean deleteByIdUser(String id);

    boolean selectUserPayPassword(String id,Integer password);

}
