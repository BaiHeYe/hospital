package com.rf.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 挂号表
 *
 * @TableName sys_registration
 */
@TableName(value = "sys_registration")
@Data
public class Registration {
    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 所属门诊
     */
    private String deptId;

    /**
     * 医生ID
     */
    private String doctorId;

    /**
     * 挂号价格
     */
    private Integer money;

    /**
     * 剩余数量
     */
    private Integer num;

    /**
     * 状态 1上架 0下架
     */
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 逻辑删除
     */

    @TableLogic
    private Integer deleted;

//    private AdminUser adminUser;
//
//    private Depart depart;

}