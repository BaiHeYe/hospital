package com.rf.admin.domain.vague;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Description 条件查询封装实体类
 * @Author rf
 * @Date 2022/4/18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Vague {

    // 工号
    private Integer jobNum;

    // 名字
    private String name;

    // 类型
    private String Type;

    // 订单类型
    private String orderInfo;

    // 订单编号
    private String orderId;

    private String password;

    // 年龄起始
    private Integer ageBegin;

    // 年龄最后
    private Integer ageEnd;

    // 时间起始
    private Date timeBean;

    // 时间最后
    private Date timeEnd;

    // 是否认证
    private Integer certified;


}
