package com.rf.admin.service.impl;

import com.rf.admin.domain.UserAddress;
import com.rf.admin.mapper.UserAddressMapper;
import com.rf.admin.service.UserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @Description
 * @Author rf
 * @Date 2022/4/11
 */
@Service
public class UserAddressServiceImpl implements UserAddressService {

    @Autowired
    private UserAddressMapper userAddressMapper;

    /**
     * 地址分页显示
     */
    @Override
    public List<UserAddress> selectAddress(String id) {
        return userAddressMapper.selectAddress(id);
    }

    /**
     * 按照ID删除
     */
    @Override
    public boolean delByIdAddress(String id) {
        int result = userAddressMapper.deleteById(id);
        return result > 0;
    }

    /**
     * 按照ID修改
     */
    @Override
    public boolean updateByIdAddress(UserAddress userAddress) {
        userAddress.setUser(null);
        int result = userAddressMapper.updateById(userAddress);
        return result > 0;
    }

    @Override
    public boolean insertAddress(UserAddress userAddress) {
        int result = userAddressMapper.insert(userAddress);
        return result > 0;
    }


}
