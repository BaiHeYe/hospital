package com.rf.admin.service;

import com.rf.admin.domain.UserAddress;

import java.util.List;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/11
 */
public interface UserAddressService {

    /**
     * 分页
     */
    List<UserAddress> selectAddress(String id);

    /**
     * 删除地址
     */
    boolean delByIdAddress(String id);

    /**
     * 修改地址
     */
    boolean updateByIdAddress(UserAddress userAddress);

    boolean insertAddress(UserAddress userAddress);
}
