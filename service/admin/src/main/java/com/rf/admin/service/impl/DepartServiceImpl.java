package com.rf.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rf.admin.domain.Depart;
import com.rf.admin.domain.vague.Vague;
import com.rf.admin.mapper.DepartMapper;
import com.rf.admin.service.DepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rf.admin.domain.vague.Department;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_depart(部门字典表)】的数据库操作Service实现
 * @createDate 2022-04-24 15:25:57
 */
@Service
public class DepartServiceImpl implements DepartService {

    @Autowired
    private DepartMapper departMapper;

    @Override
    public Page<Depart> getPageDept(Integer pageCurrent, Integer pageSize, Vague vague) {
        Page<Depart> pageDept = new Page<>(pageCurrent, pageSize);
        LambdaQueryWrapper<Depart> wq = new LambdaQueryWrapper<>();
        wq.like(StringUtils.isNotBlank(vague.getName()), Depart::getDepartName, vague.getName());
        return departMapper.selectPage(pageDept, null);
    }

    @Override
    public List<Department> getAllDept() {
        return departMapper.getAll();
    }

    @Override
    public boolean delDept(String id) {
        return departMapper.deleteById(id) > 0;
    }

    @Override
    public boolean updateDept(Depart depart) {
        return departMapper.updateById(depart) > 0;
    }

    @Override
    public boolean insertDept(Depart depart) {
        return departMapper.insert(depart) > 0;
    }
}
