package com.rf.admin.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rf.admin.domain.Order;
import com.rf.admin.domain.User;
import com.rf.admin.domain.vague.Vague;
import com.rf.admin.mapper.OrderMapper;
import com.rf.admin.mapper.UserMapper;
import com.rf.admin.service.OrderService;
import com.rf.admin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_order(订单表)】的数据库操作Service实现
 * @createDate 2022-04-23 09:23:49
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private UserService userService;

    @Override
    public List<Order> getOrderList(String orderInfo,String userName,String orderId) {
        return orderMapper.selectOrder(orderInfo,userName,orderId);
    }

    @Override
    public List<Order> getOutOrderList(String userName) {
        return orderMapper.selectOutOrder(userName);
    }

    @Override
    public boolean outOrder(String orderId) {
        // 出库status为1
        Order order = new Order();
        order.setId(orderId);
        order.setOrderStatus(1);
        return orderMapper.updateById(order) > 0;
    }

    @Override
    public List<Order> getRegistrationList(String token) {
        return orderMapper.selectRegistration(token);
    }

    @Override
    public boolean insertOrder(Order order) {
        return orderMapper.insert(order) > 0;
    }

    @Override
    public boolean payPassword(Order order) {
        boolean b = userService.selectUserPayPassword(order.getUser().getId(), order.getUser().getPayPassword());
        if (b){
            Order order1 = new Order();
            order1.setPayStatus(1);
            order1.setId(order.getId());
            return orderMapper.updateById(order1) > 0;
        }
        return false;
    }

}
