package com.rf.admin.service;


import com.rf.admin.domain.Order;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_order(订单表)】的数据库操作Service
 * @createDate 2022-04-23 09:23:49
 */
public interface OrderService {

    List<Order> getOrderList(String orderInfo,String userName,String orderId);

    List<Order> getOutOrderList(String userId);

    boolean outOrder(String orderId);

    List<Order> getRegistrationList(String token);

    boolean insertOrder(Order order);

    boolean payPassword(Order order);
}
