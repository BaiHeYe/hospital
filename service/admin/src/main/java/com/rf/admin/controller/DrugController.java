package com.rf.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rf.admin.domain.Drug;
import com.rf.admin.domain.vague.Vague;
import com.rf.admin.service.DrugService;
import com.rf.common.response.ResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/17
 */

@RestController
@RequestMapping("/drug")
public class DrugController {

    @Autowired
    private DrugService drugService;

    @PostMapping("/{current}/{size}")
    public ResponseVo pageDrug(@PathVariable Integer current,
                               @PathVariable Integer size,
                               Vague vague) {
        Page<Drug> drugPage = drugService.pageDrug(current, size, vague);
        return ResponseVo.SUCCESS(drugPage);
    }

    @DeleteMapping("/{id}")
    public ResponseVo delDrug(@PathVariable String id) {
        return drugService.delDrug(id) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @PostMapping
    public ResponseVo saveDrug(@RequestBody Drug drug) {
        return drugService.insertDrug(drug) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @PutMapping
    public ResponseVo updateDrug(@RequestBody Drug drug) {
        return drugService.updateDrug(drug) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @GetMapping("allDrug")
    public ResponseVo getAllDrug(){
        return ResponseVo.SUCCESS(drugService.getAllDrug());
    }

}
