package com.rf.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rf.admin.domain.AdminUser;
import com.rf.admin.domain.vague.Vague;
import com.rf.admin.service.AdminUserService;
import com.rf.common.response.ResponseVo;
import com.rf.common.utils.QiNiuYunUpFileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/14
 */
@RestController
@RequestMapping("/admin")
public class AdminUserController {

    @Autowired
    private AdminUserService adminUserService;

    @PostMapping("/{pageCurrent}/{pageSize}")
    public ResponseVo getAdminPage(@PathVariable Integer pageCurrent,
                                   @PathVariable Integer pageSize,
                                   Vague vague) {
        Page<AdminUser> adminUserPage = adminUserService.pageAdminUser(pageCurrent, pageSize, vague);
        return ResponseVo.SUCCESS(adminUserPage);
    }

    @PostMapping("/uploadImage")
    public ResponseVo uploadImage(MultipartFile file) {
        return QiNiuYunUpFileUtils.getUpLoad(file);
    }


    @DeleteMapping("/{id}")
    public ResponseVo delAdmin(@PathVariable String id) {
        return adminUserService.delByIdAdminUser(id) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @PutMapping
    public ResponseVo updateAdmin(@RequestBody AdminUser adminUser) {
        return adminUserService.updateByIdAdminUser(adminUser) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @PostMapping
    public ResponseVo insertAdmin(@RequestBody AdminUser adminUser) {
        return adminUserService.insertAdminUser(adminUser) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @GetMapping("/idanduser")
    public ResponseVo getIdAndName(){
        return ResponseVo.SUCCESS( adminUserService.getIdAndName());
    }
}
