package com.rf.admin.controller;

import com.rf.admin.domain.Order;
import com.rf.admin.domain.vague.Vague;
import com.rf.admin.service.OrderService;
import com.rf.common.response.ResponseVo;
import com.rf.common.utils.JwtUtils;
import com.rf.common.utils.QRCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/23
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping
    public ResponseVo listOrder(@RequestBody Vague vague) {
        List<Order> orderList = orderService.getOrderList(vague.getOrderInfo(),vague.getName(),vague.getOrderId());
        return ResponseVo.SUCCESS(orderList);
    }

    @GetMapping("/out")
    public ResponseVo listOutOrder(String userName) {
        List<Order> orderList = orderService.getOutOrderList(userName);
        return ResponseVo.SUCCESS(orderList);
    }

    @GetMapping("/out/{orderId}")
    public ResponseVo outOrder(@PathVariable String orderId) {
        return orderService.outOrder(orderId) ? ResponseVo.SUCCESS : ResponseVo.FAIL;
    }

    @GetMapping("/registration")
    public ResponseVo registerOrder(HttpServletRequest token){
        if (JwtUtils.checkToken(token)){
            String tokenId = JwtUtils.getMemberIdByJwtToken(token);
            return ResponseVo.SUCCESS(orderService.getRegistrationList(tokenId));
        }
        return ResponseVo.FAIL("不是有效token");
    }

    /**
     * 支付二维码
     */
    @PostMapping("/getQRCode")
    public ResponseVo payOrder(@RequestBody Order orderId){
        String base64QRCode = QRCodeUtil.getBase64QRCode("http://192.168.8.108/#/pay?orderid=" + orderId.getOrderId());
        System.out.println("http://192.168.8.108/#/pay?orderid=" + orderId.getOrderId());
        return ResponseVo.SUCCESS(base64QRCode);
    }

    /**
     * 支付
     */
    @PostMapping("/pay")
    public ResponseVo payPassword(@RequestBody Order order){
        return orderService.payPassword(order)? ResponseVo.SUCCESS : ResponseVo.FAIL("密码错误");
    }

    /**
     * 退款
     */
//    @PostMapping("/outpay")
//    public ResponseVo outPayOrder(@RequestBody Order order){
//
//
//    }
}
