package com.rf.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rf.admin.domain.Order;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_order(订单表)】的数据库操作Mapper
 * @createDate 2022-04-23 09:23:49
 * @Entity com.rf.admin.domain.Order
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {

    List<Order> selectOrder(String orderInfo,String userName,String orderId);

    List<Order> selectOutOrder(String userName);

    List<Order> selectRegistration(String token);
}
