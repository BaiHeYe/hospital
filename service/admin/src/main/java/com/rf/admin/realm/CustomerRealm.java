package com.rf.admin.realm;


import com.rf.admin.domain.AdminUser;
import com.rf.admin.service.AdminUserService;
import com.rf.common.utils.JwtUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.Map;


/**
 * @Description 自定义realm
 * @Author rf
 * @Date 2022/4/17
 */
@Component
public class CustomerRealm extends AuthorizingRealm {

    @Autowired
    private AdminUserService admin;

    private static String jwtToken;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        String principal = (String) token.getPrincipal();
        AdminUser adminUser = admin.findByAdminName(principal);
        jwtToken = JwtUtils.getJwtToken(adminUser.getId(), adminUser.getAdminName());
        if (!ObjectUtils.isEmpty(adminUser)) {
            return new SimpleAuthenticationInfo(adminUser.getAdminName(), adminUser.getAdminPassword(), ByteSource.Util.bytes(adminUser.getSalt()), this.getName());
        }
        return null;
    }

    public static String getIdAndName(){
        return jwtToken;
    }
}
