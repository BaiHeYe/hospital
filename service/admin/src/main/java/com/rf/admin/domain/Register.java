package com.rf.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 用户挂号表
 *
 * @TableName sys_register
 */
@Data
public class Register {
    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 产品id
     */
    private String productId;

    /**
     * 产品数量
     */
    private Integer productNum;

    /**
     * 总价格
     */
    private Integer money;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @TableLogic
    private Integer deleted;

    private Drug drug;
}