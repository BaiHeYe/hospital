package com.rf.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/11
 */

@SpringBootApplication
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.rf"})  // 扫描com.rf下都的注解会扫描
public class AdminMain {

    public static void main(String[] args) {
        SpringApplication.run(AdminMain.class, args);

    }
}
