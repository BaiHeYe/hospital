package com.rf.admin.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rf.admin.domain.Drug;
import com.rf.admin.domain.vague.Vague;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_drug(药品表)】的数据库操作Service
 * @createDate 2022-04-17 19:30:43
 */
public interface DrugService {

    Page<Drug> pageDrug(Integer pageCurrent, Integer pageSize, Vague vague);

    boolean delDrug(String id);

    boolean insertDrug(Drug drug);

    boolean updateDrug(Drug drug);

    List<Drug> getAllDrug();

    boolean updateDrugNumber(String id,Integer number);
}
