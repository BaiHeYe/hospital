package com.rf.admin.service;

import com.rf.admin.domain.Register;

import java.util.List;

/**
 * @author Mr.R
 * @description 针对表【sys_register(用户挂号表)】的数据库操作Service
 * @createDate 2022-04-21 20:28:39
 */
public interface RegisterService {

    List<Register> getRegisters(String orderId);

    boolean delByIdRRegister(String id);

    boolean updateRegister(String id);

    boolean saveRegister(String userId,Register[] registers) throws Exception;

}
