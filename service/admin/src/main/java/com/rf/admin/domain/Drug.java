package com.rf.admin.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * 药品表
 *
 * @TableName sys_drug
 */
@Data
public class Drug {
    /**
     * 药品ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String Id;

    /**
     * 药品名称
     */
    private String drugName;

    /**
     * 药品类型
     */
    private String drugType;

    /**
     * 药品简介
     */
    private String drugSuggest;


    /**
     * 药品价格
     */
    private int drugMoney;

    /**
     * 药品数量
     */
    private int drugNum;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 最后更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer deleted;

    private String drugImg;
}