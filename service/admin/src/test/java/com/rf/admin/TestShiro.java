package com.rf.admin;

import com.rf.admin.mapper.OrderMapper;
import com.rf.admin.mapper.RegistrationMapper;
import com.rf.admin.mapper.UserMapper;
import com.rf.admin.service.RegistrationService;
import com.rf.common.utils.MailUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.mail.MessagingException;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/16
 */
@SpringBootTest
public class TestShiro {


    @Autowired
    private RegistrationService r;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RegistrationMapper registrationMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private MailUtils mailUtils;

    @Autowired
    private RedisTemplate redisTemplate;
    @Test
    void test() throws MessagingException {
        mailUtils.contextLoads("347684223@qq.com");
    }


}
