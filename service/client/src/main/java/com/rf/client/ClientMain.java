package com.rf.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/17
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.rf"})
public class ClientMain {

    public static void main(String[] args) {
        SpringApplication.run(ClientMain.class, args);
    }

}
