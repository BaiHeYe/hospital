package com.rf.client.service;

import com.rf.client.domain.User;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/17
 */
public interface RegisterUserService {

    boolean register(String username, String password);

    User findByUsername(String username);
}
