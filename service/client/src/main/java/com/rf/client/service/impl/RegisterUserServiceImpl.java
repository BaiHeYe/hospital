package com.rf.client.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rf.client.domain.User;
import com.rf.client.mapper.UserMapper;
import com.rf.client.service.RegisterUserService;
import com.rf.common.utils.Md5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description 用户登录注册的service
 * @Author rf
 * @Date 2022/4/17
 */
@Service
public class RegisterUserServiceImpl implements RegisterUserService {


    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean register(String username, String password) {
        System.out.println(username + password);
        String salt = Md5Utils.getSalt(8);
        String md5 = Md5Utils.getMd5(password, salt);
        User user = new User();
        user.setName(username);
        user.setPassword(md5);
        user.setSalt(salt);
        return userMapper.insert(user) > 0;
    }

    @Override
    public User findByUsername(String username) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("name", username);
        return userMapper.selectOne(wrapper);
    }


}
