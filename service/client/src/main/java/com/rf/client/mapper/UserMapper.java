package com.rf.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rf.client.domain.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/11
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {


}
