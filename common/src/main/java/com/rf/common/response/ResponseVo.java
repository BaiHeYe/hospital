package com.rf.common.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description 国际化0
 * @Author rf
 * @Date 2022/4/2
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseVo {
    private String code;
    private String msg;
    private Object data;

    public static ResponseVo SUCCESS = new ResponseVo("200", "success", null);

    public static ResponseVo FAIL = new ResponseVo("500", "fail", null);

    public static ResponseVo SUCCESS(Object data) {
        return new ResponseVo("200", "success", data);
    }

    public static ResponseVo FAIL(Object data) {
        return new ResponseVo("500", "fail", data);
    }
}
