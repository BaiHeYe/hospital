package com.rf.common.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Description 日期自动填充
 * @Author rf
 * @Date 2022/4/10
 */
@Component  // 交给spring管理
public class MyMetaObjectHandler implements MetaObjectHandler {

    /**
     * 添加的时候按照名称自动填充日期
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        // 根据名称设置属性值
        this.setFieldValByName("createTime", new Date(), metaObject);
        this.setFieldValByName("updateTime", new Date(), metaObject);

//        this.setFieldValByName("photographPath","http://r9nllsa01.hb-bkt.clouddn.com/17b4cfdf-6658-4836-9128-51070644d7fb-%E6%B8%B8%E6%88%8F%E6%9C%BA%E7%94%B7%E5%AD%A9.png",metaObject);
    }

    /**
     * 修改的时候按照名称自动填充日期
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime", new Date(), metaObject);
    }
}