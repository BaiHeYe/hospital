package com.rf.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description 验证类
 * @Author rf
 * @Date 2022/5/1
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Email {

    private String email;

    private String emailCode;

    private String password;


}
