package com.rf.common.utils;

import java.util.Scanner;
import java.util.UUID;

/**
 * @Description 生成uuid
 * @Author rf
 * @Date 2022/5/6
 */
public class UUIDUtils {

    public static String getId(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

}
