package com.rf.common.utils;

import org.apache.shiro.crypto.hash.Md5Hash;

import java.util.Random;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/17
 */
public class Md5Utils {

    /**
     * 处理密码加密
     */
    public static String getMd5(String password, String salt) {
        // 明文密码 + md5 + salt + hash散列
        return new Md5Hash(password, salt, 1024).toHex();
    }

    /**
     * 生成盐
     */
    public static String getSalt(int n) {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            char aChar = chars[new Random().nextInt(chars.length)];
            sb.append(aChar);
        }
        return sb.toString();
    }


}
