package com.rf.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Description
 * @Author rf
 * @Date 2022/4/12
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket getDoc() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("rf")
                .select()//扫描配置信息
                //配置扫描方式
                .apis(RequestHandlerSelectors.basePackage("com.rf"))
                .build();
    }

}
